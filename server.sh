#!/usr/bin/env bash
echo "Starting up server"
export FLASK_APP=manage.py
export FLASK_ENV=development
flask run --host=0.0.0.0
