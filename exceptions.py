class CustomException(BaseException):
    def __init__(self, error_message: str):
        self._error = error_message

    @property
    def error_message(self):
        return self._error
