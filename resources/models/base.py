from datetime import datetime

from app import db
from exceptions import CustomException


class BaseMixin(object):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow())
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow,
                           default=datetime.utcnow())

    def save(self):
        db.session.flush()
        db.session.add(self)
        db.session.commit()
        return self

    @classmethod
    def create(cls, param,):
        resource_instance = cls(**param)
        resource_instance.save()
        return resource_instance.to_dict()

    @classmethod
    def destroy(cls, resource_id):
        resource = cls.query.get(resource_id)
        if not resource:
            raise CustomException(f'The {cls.__name__} you are trying to delete does not exist')
        db.session.delete(resource)
        db.session.commit()

    def to_dict(self):
        result = {}
        for column in self.__table__.columns:
            result[column.name] = str(getattr(self, column.name))
        return result

    @classmethod
    def get(cls, resource_id):
        resource = cls.query.get(resource_id)
        if resource:
            return resource
