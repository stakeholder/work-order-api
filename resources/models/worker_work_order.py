from app import db


worker = db.Column('worker_id', db.Integer, db.ForeignKey('workers.id'),
                   primary_key=True)
work_order = db.Column('work_order_id', db.Integer,
                       db.ForeignKey('work_orders.id'), primary_key=True)

workers_work_order = db.Table('workers_work_order', worker, work_order)
