from .base import BaseMixin
from .work_order import WorkOrder
from .worker_work_order import workers_work_order
from app import db
from exceptions import CustomException


class Worker(BaseMixin, db.Model):
    __tablename__ = 'workers'
    firstname = db.Column(db.String(20), nullable=False)
    surname = db.Column(db.String(20), nullable=False)
    company_name = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(20), nullable=False)
    work_orders = db.relationship('WorkOrder', secondary=workers_work_order, lazy=True,
                                  backref=db.backref('workers', lazy=True))
    __table_args__ = (db.UniqueConstraint('surname', 'firstname', 'email', 'company_name'), )

    @classmethod
    def create(cls, param):
        if cls.query.filter_by(**param).first():
            raise CustomException("The Worker you are trying to add already exist")
        return super().create(param)

    @classmethod
    def fetch_work_orders(cls, worker_id):
        worker = cls.get(worker_id)
        if not worker:
            raise CustomException("The Worker does not exist")
        result = WorkOrder.query.with_parent(worker).order_by('deadline').all()
        return [value.to_dict() for value in result]
