from .base import BaseMixin
from app import db
from exceptions import CustomException


class WorkOrder(BaseMixin, db.Model):
    __tablename__ = 'work_orders'
    title = db.Column(db.String(20), nullable=False, unique=True)
    description = db.Column(db.String(), nullable=True)
    deadline = db.Column(db.DateTime, nullable=False)

    @classmethod
    def create(cls, param):
        if cls.query.filter_by(title=param['title']).first():
            raise CustomException("The Work Order you are trying to add already exist")
        return super().create(param)
