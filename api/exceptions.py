from .errors import (
    validation_error,
    internal_server_error
)


class ValidationException(BaseException):
    def __init__(self, error_message, propagate=False):
        self.error = error_message
        self.propagate = propagate

    @property
    def should_propagate(self):
        return self.propagate

    def broadcast(self):
        return validation_error(self.error)


class ServerException(BaseException):
    def __init__(self, error_message):
        self.error = error_message

    def broadcast(self):
        return internal_server_error(self.error)
