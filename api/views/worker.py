from flask import request

from api.concepts.worker.operation import (
    WorkerPost,
    WorkerDelete,
    WorkerWorkOrderAssignment,
    WorkerWorkOrders
)
from api.decorators import catch_exceptions
from api.utils import respond_to_json


class Worker:

    @staticmethod
    @catch_exceptions('Worker creation failed')
    def post():
        response = WorkerPost.process(request)
        return respond_to_json(data=response, status=201)

    @staticmethod
    @catch_exceptions("An error occured while trying to delete a worker")
    def delete(id):
        WorkerDelete.process(request, id)
        return respond_to_json()

    @staticmethod
    @catch_exceptions("An error occured while trying to assign a worker to a work order")
    def assign_work_order(id):
        WorkerWorkOrderAssignment.process(request, id)
        return respond_to_json()

    @staticmethod
    @catch_exceptions("An error occured while trying to fetch work orders")
    def fetch_work_orders(id):
        response = WorkerWorkOrders.process(request, id)
        return respond_to_json(data=response, status=200)
