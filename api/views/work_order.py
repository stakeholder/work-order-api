from flask import request

from api.concepts.work_order.operation import WorkOrderPost
from api.decorators import catch_exceptions
from api.utils import respond_to_json


class WorkOrder:

    @staticmethod
    @catch_exceptions('Work Order creation failed')
    def post():
        response = WorkOrderPost.process(request)
        return respond_to_json(data=response, status=201)
