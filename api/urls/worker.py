from api import api
from api.views.worker import Worker


api.add_url_rule('/workers', endpoint='Worker_post', view_func=Worker.post,
                 methods=['POST'], strict_slashes=False)

api.add_url_rule('/workers/<id>', endpoint='Worker_delete', view_func=Worker.delete,
                 methods=['DELETE'], strict_slashes=False)

api.add_url_rule('/workers/<id>/work-orders', endpoint='Worker_work_order',
                 view_func=Worker.assign_work_order, methods=['POST'],
                 strict_slashes=False)

api.add_url_rule('/workers/<id>/work-orders', endpoint='Worker_get_work_orders',
                 view_func=Worker.fetch_work_orders, methods=['GET'],
                 strict_slashes=False)
