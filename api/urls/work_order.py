from api import api
from api.views.work_order import WorkOrder


api.add_url_rule('/work-orders', endpoint='WorkOrder_post', view_func=WorkOrder.post,
                 methods=['POST'], strict_slashes=False)
