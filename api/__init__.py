from flask import Blueprint

api = Blueprint('api', __name__)
from . import (
    urls,
    views,
    utils,
    exceptions,
    errors,
    decorators
)
