from api.utils import validate_request


class Post:

    @classmethod
    def process(cls, request_object, model, required_params=None, keys_to_cast=None, acceptable_params=None):
        param = validate_request(request_object, acceptable_param_keys=acceptable_params,
                                 required_param_keys=required_params, keys_to_cast_to_int=keys_to_cast)
        return model.create(param)
