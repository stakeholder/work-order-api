from api.utils import validate_request


class Delete:

    @classmethod
    def process(cls, request_object, resource_id, model):
        validate_request(request_object)
        return model.destroy(resource_id)
