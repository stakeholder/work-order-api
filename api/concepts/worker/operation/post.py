from api.concepts.base.operation import Post
from resources.models import Worker


class WorkerPost(Post):

    @classmethod
    def process(cls, request_object):
        required_params = ['surname', 'firstname', 'email', 'company_name']
        acceptable_params = ['surname', 'firstname', 'email', 'company_name']
        return super().process(request_object, Worker, required_params=required_params,
                               acceptable_params=acceptable_params)
