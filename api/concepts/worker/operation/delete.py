from api.concepts.base.operation import Delete
from resources.models import Worker


class WorkerDelete(Delete):

    @classmethod
    def process(cls, request_object, resource_id):
        return super().process(request_object, int(resource_id), Worker)
