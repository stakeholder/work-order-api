from api.concepts.base.operation import Post
from api.utils import validate_request
from exceptions import CustomException
from resources.models import (
    Worker,
    WorkOrder
)


class WorkerWorkOrderAssignment:

    @classmethod
    def process(cls, request_object, worker_id):
        required_params = ['work_order_id']
        acceptable_params = ['work_order_id']
        keys_to_cast = ['work_order_id']
        params = validate_request(request_object, required_param_keys=required_params,
                                  acceptable_param_keys= acceptable_params, keys_to_cast_to_int=keys_to_cast)

        work_order_id = params['work_order_id']
        worker = Worker.get(int(worker_id))

        if not worker:
            raise CustomException('The worker you are trying to assign a worker order does not exist')

        work_order = WorkOrder.get(work_order_id)

        if not work_order:
            raise CustomException('The work order you are trying to assign a worker does not exist')

        if len(worker.work_orders) == 5:
            raise CustomException('The maximum number of workers for this work order has been reached')

        if Worker.query.with_parent(work_order).filter_by(id=worker.id).first():
            raise CustomException('This worker has already been added to the work order')

        worker.work_orders.append(work_order)
        worker.save()
