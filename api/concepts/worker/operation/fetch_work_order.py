from api.utils import validate_request
from resources.models import Worker


class WorkerWorkOrders:

    @classmethod
    def process(cls, request_object, worker_id):
        validate_request(request_object)
        return Worker.fetch_work_orders(worker_id)
