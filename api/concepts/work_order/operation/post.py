from api.concepts.base.operation import Post
from resources.models import WorkOrder


class WorkOrderPost(Post):

    @classmethod
    def process(cls, request_object):
        required_params = ["title", "deadline"]
        acceptable_params = ["title", "description", "deadline"]
        return super().process(request_object, WorkOrder, required_params=required_params,
                               acceptable_params=acceptable_params)
