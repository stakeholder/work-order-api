from flask import Flask
from flasgger import Swagger
from flask_sqlalchemy import SQLAlchemy

from config import config

db = SQLAlchemy()


def create_app(environment):
    app = Flask(__name__)
    app.config.from_object(config[environment])
    db.init_app(app)

    from api import api
    from resources import resources
    app.register_blueprint(api)
    app.register_blueprint(resources)
    Swagger(app)
    return app
