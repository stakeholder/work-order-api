# Work Order API
Work Order Service is a REST API that provides workers and work order related information.

## Development
* [Flask](http://flask.pocoo.org/) - Flask is a BCD licensed microframework for Python based on Werkzeug and Jinja 2.
* [Python-dotenv](https://github.com/theskumar/python-dotenv) - This library allows the saving of environment variables in .env and loaded when the application runs.
* [SQLAlchemy](https://www.fullstackpython.com/sqlalchemy.html) - This library is a Python distribution containing tools for working with sql databases.

## Installation
#### In Docker Environment
1. Start up your terminal (or Command Prompt on Windows OS).
2. Clone the repository by entering the command `https://gitlab.com/stakeholder/work-order-api.git` in the terminal.
3. Create a `.env` and `database.env` file in your root directory as described in `.env.example` and `database.env.example` file and configure the necessary options. It is essential you create this file before running the application.
4. Build a docker image by running the command `docke-compose up`
5. Enter into the python container and setup up your database following these steps:
    * `python manage.py db init`
    * `python manage.py db migrate`
    * `python manage.py db upgrade`


#### Outside Docker Environment
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `python 3` installed on your PC.
3. Clone the repository by entering the command `https://gitlab.com/stakeholder/work-order-api.git` in the terminal.
4. Create a `.env` and `database.env` file in your root directory as described in `.env.example` and `database.env.example` file and configure the necessary options. It is essential you create this file before running the application.
5. Install requirements by running the command `pip3 install -r requirements_dev.txt`
6. Start local server by running the command.
    * For unix systems:
      1. `export FLASK_APP=manage.py`
      2.  `flask run -h "0.0.0.0" -p 5000`  
    * For windows systems:
      1. `set FLASK_APP=manage.py`
      2.  `flask run -h "0.0.0.0" -p 5000`
7. Setup up your database following these steps:
    * `python manage.py db init`
    * `python manage.py db migrate`
    * `python manage.py db upgrade`


### API Resource Endpoints
URL Prefix = Base URL is `http://localhost:5000` on your local system and `https://work-order-api.herokuapp.com`` on heroku

Header format = `{"Content-Type": "application/json"}`


| EndPoint                                 | Functionality                 | Parameters|
| -----------------------------------------|-----------------------------|:-------------:|
| **POST** `/workers`            | Create a worker            |    `body_params = [company_name, email, firstname, surname]`    |
| **DELETE** `/workers/<worker_id>`        | Delete a worker  |    `path_param = worker_id`     |
| **POST** `/workers/<worker_id>/work-orders`           | Assign a worker to a work order  |    `body_params=[work_order_id]`, `path_param=worker_id`  |
| **GET** `/workers/<worker_id>/work-orders`         | Fetch all work orders for a specific worker   |   `path_param=worker_id`    |
| **POST** `/work-orders`       | Create a work order       |  `body_params = [title, description, deadline]`      |

## Authors

**Koya Adegboyega Gabriel.**
